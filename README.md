# Camera UI

## motion.conf

###  output_pictures is off
After motion server startup the `output_pictures` is set to `on` after a short time to allow the camera to get used to current light conditions.
It is to prevent the ~20 snapshots at each start of the motion server.

## Camera service
As root
```
$ cp ./camera-server.service /etc/systemd/system/
$ systemctl daemon-reload
$ systemctl enable camera-server
```

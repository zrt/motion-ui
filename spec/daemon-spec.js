'use strict';

const nock = require('nock');
const { Daemon } = require('../src/daemon.js');

describe('Daemon', () => {
	let daemon, options;

	beforeEach(() => {
		options = { 
			startWaitMs: 50,
			startWaitIntervalMs: 50,
			waitForCameraMs: 10,
			exec: jasmine.createSpy('exec')
		}
		daemon = new Daemon(options);
	})

	describe('isDetectionActive', () => {
		it('should return true if status is ACTIVE', (done) => {
			nock('http://127.0.0.1:8082')
				.get('/0/detection/status')
				.reply(200, 'ACTIVE');
			
			daemon.isDetectionActive()
				.then((status) => expect(status).toBe(true))
				.then(done);
		})
		
		it('should return false if status is not ACTIVE', (done) => {
			nock('http://127.0.0.1:8082')
				.get('/0/detection/status')
				.reply(200, 'PAUSED');

			daemon.isDetectionActive()
				.then((status) => expect(status).toBe(false))
				.then(done);
		})

		it('should reject if no response', (done) => {
			daemon.isDetectionActive()
				.catch(done)
		})
	})

	describe('setDetection', () => {
		it('should request /start for true', (done) => {
			nock('http://127.0.0.1:8082')
				.get('/0/detection/start')
				.reply(200);
			
			daemon.setDetection(true)
				.then(nock.isDone)
				.then(done)
		})

		it('should request /pause for true', (done) => {
			nock('http://127.0.0.1:8082')
				.get('/0/detection/pause')
				.reply(200);
			
			daemon.setDetection(false)
				.then(nock.isDone)
				.then(done)
		})
	})

	describe('start', () => {
		it('should disable detection right after started', (done) => {
			nock('http://127.0.0.1:8081').head('/').replyWithError('omg')
			nock('http://127.0.0.1:8081').head('/').reply(200)
			nock('http://127.0.0.1:8082').get('/0/detection/pause').reply(200)
			nock('http://127.0.0.1:8082').get('/0/config/set?output_pictures=on').reply(200)

			options.exec.and.returnValue(Promise.resolve());

			daemon.start()
				.then(() => expect(options.exec).toHaveBeenCalledWith('sudo service motion restart'))
				.then(() => nock.isDone())
				.then(done)
		})
	})
});
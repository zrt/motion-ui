'use strict';

const nock = require('nock');
const { Controller } = require('../src/controller.js');

describe('Controller', () => {
	let controller, daemon;
	const options = {
			stopTimeMs: 10
		}

	beforeEach(() => {
		daemon = jasmine.createSpyObj('daemon', ['start', 'stop', 'isRunning', 'isDetectionActive', 'setDetection']);
		controller = new Controller(daemon, options);
	})



	describe('initialize', () => {

		/* note: detection status false indicates daemon is running */
		it('should call daemon stop if running and no detection is active', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(false));
			daemon.stop.and.callFake(done);

			controller.initialize();
		})

		it('should not call daemon stop if detection is active', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(true));
			daemon.stop.and.callFake(() => { fail('Fail') });
			setTimeout(done, options.stopTimeMs * 10);
			controller.initialize()
		})

		it('should not call daemon stop if daemon is not running', (done) => {
			// there is no detectionquery set - resulting error
			daemon.isDetectionActive.and.returnValue(Promise.reject());
			daemon.stop.and.callFake(() => { fail('Fail') });
			setTimeout(done, options.stopTimeMs * 10);
			controller.initialize()
		})
	})

	describe('isDetectionActive', () => {

		it('should return true if status is ACTIVE', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(true));

			controller.isDetectionActive()
				.then((status) => expect(status).toBe(true))
				.then(done);
		})
		
		it('should return false if status is not ACTIVE', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(false));

			controller.isDetectionActive()
				.then((status) => expect(status).toBe(false))
				.then(done);
		})

		it('should return false if no response', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.reject());

			controller.isDetectionActive()
				.then((status) => expect(status).toBe(false))
				.then(done);
		})
	})

	describe('when live stopped', () => {
		beforeEach((done) => {
			daemon.start.and.returnValue(Promise.resolve());
			controller.livePing()
				.then(done);
		});

		it('should not stop service if detection is in progress', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(true));
			daemon.stop.and.callFake(() => { fail('Fail') });
			setTimeout(done, options.stopTimeMs * 10);
		});

		it('should stop service if no detection', (done) => {
			daemon.isDetectionActive.and.returnValue(Promise.resolve(false));
			daemon.stop.and.callFake(done);
		});
	})

	describe('when detection stopped', () => {
		it('should stop service if no live', (done) => {
			daemon.isRunning.and.returnValue(Promise.resolve(true));
			daemon.stop.and.callFake(done);
			daemon.isDetectionActive.and.returnValue(Promise.resolve(false));

			controller.setDetection(false);
		})

		it('should not stop service if live in progress', (done) => {
			daemon.start.and.returnValue(Promise.resolve());
			daemon.isRunning.and.returnValue(Promise.resolve(true));
			daemon.isDetectionActive.and.returnValue(Promise.resolve(true));

			controller.setDetection(false)
				.then(() => new Promise((resolve) => setTimeout(() => { controller.livePing(); resolve(); }, options.stopTimeMs / 2)))
				.then(() => new Promise((resolve) => setTimeout(() => { controller.livePing(); resolve(); }, options.stopTimeMs / 2)))
				.then(() => new Promise((resolve) => setTimeout(() => { controller.livePing(); resolve(); }, options.stopTimeMs / 2)))
				.then(() => expect(daemon.stop).not.toHaveBeenCalled())
				.then(done);
		})
	})
})
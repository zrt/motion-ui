'use strict';

var express = require('express')
var fs = require('fs')
var path = require('path')
var app = express()
const { Daemon } = require('./daemon.js');
const { Controller } = require('./controller.js');


const image_dir = '/var/lib/motion/';

const daemon = new Daemon();
const motion = new Controller(daemon);

app.get('/camera/images', function(req, res) {
	let files = fs.readdirSync(image_dir)
		.map(function(v) {
			return { name: v,
					 timestamp: fs.statSync(image_dir + v).mtime.getTime() / 1000 }
		})
		.sort(function(a, b) { return a.timestamp - b.timestamp; })

	res.contentType('application/json')
		.json(files);
});

app.get('/camera/status', function(req,res) {
	let response = { detection: false };
	motion.isDetectionActive()
		.then((v) => {
			response.detection = v; 
			res.send(response);
		})
		.catch((e) => res.status(500).send(e) );
});

app.post('/camera/detection/on', function(req,res) {
	motion.setDetection(true)
		.then(() => res.send())
		.catch((e) => res.status(500).send(e));
});

app.post('/camera/detection/off', function(req,res) {
	motion.setDetection(false)
		.then(() => res.send())
		.catch((e) => res.status(500).send(e));
});

app.post('/camera/live-ping', (req, res) => {
	motion.livePing()
		.then(() => res.status(200))
		.catch((e) => res.status(500))
		.then(() => res.send())
});

app.get('/camera/images/:imageid', function (req, res) {
	res.sendFile(image_dir + req.params.imageid);
})

app.use('/camera/', express.static(path.join(__dirname, '../www')))

motion.initialize()
	.then(() => {
		console.log("Listening on 3000...");
		app.listen(3000);
	})
	.catch((e) => {
		console.error(e);
	});

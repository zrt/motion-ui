'use strict';

const requestpromise = require('request-promise-native');



class Controller {
	constructor(daemon, options) {
		this.daemon = daemon;
		this.options = Object.assign({}, {
			stopTimeMs: 2000
		}, options);
		this.timeoutId = null;
	}

	_startStopTimer() {
		if (this.timeoutId) {
			clearTimeout(this.timeoutId);
		}
		console.log(`Starting stop timer with ${this.options.stopTimeMs} ms`);
		this.timeoutId = setTimeout(() => { this._stopCallback() }, this.options.stopTimeMs);
	}

	_stopCallback() {
		this.isDetectionActive()
			.then((detecting) => {
				console.log('detecting -> ', detecting);
				if (!detecting) {
					this.daemon.stop();
				}
			})
	}

	initialize() {
		return this.daemon.isDetectionActive()
			.then(status => {
				console.log("Detection status is", status);
				if (!status) {
					this._startStopTimer();
				}
			})
			.catch(() => {
				console.log('Motion not running. Detection is off')
			})
	}

	isDetectionActive() {
		return this.daemon.isDetectionActive()
			.catch(() => false);
	}

	setDetection(enabled) {
		console.log(`Set detection to ${enabled}`)
		return this.daemon.isRunning()
			.then((isRunning) => {
				if (!isRunning && enabled) {
					return this.daemon.start();
				}
			}).then(() => {
				if (!enabled) {
					this._startStopTimer();
				}
				return this.daemon.setDetection(enabled);
			});
	}

	/**
	 * Called every 5secs when live stream is being viewed
	 * @return promise when live stream is viewable
	 */
	livePing() {
		return this.daemon.start()
			.then(() => this._startStopTimer());
	}
};

module.exports.Controller = Controller;
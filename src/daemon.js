'use strict';

const exec = require('child-process-promise').exec;
const requestpromise = require('request-promise-native');


const MOTION_WEB_URL = 'http://127.0.0.1:8082';
const MOTION_STREAM_URL = 'http://127.0.0.1:8081';



class Daemon {
	constructor(options) {
		this.options = Object.assign({}, { 
			startWaitMs: 5000,
			startWaitIntervalMs: 500,
			waitForCameraMs: 2000,
			exec: exec
		}, options)
	}

	/**
	 * Checks if daemon is running
	 * @return a promise which is resovled with a boolean of running-status
	 */
	isRunning() {
		return requestpromise({
				method: 'HEAD',
				uri: MOTION_STREAM_URL
			})
			.then(() => true)
			.catch(() => false);
	}

	/**
	 * Starts motion daemon, waits until live stream comes alive
	 * Sets detection status to false automatically
	 * @return a promise
	 */
	start() {
		return this.isRunning()
					.then((status) => {
						if (status) {
							return;
						}

						console.log("start motion service");
						return this.options.exec('sudo service motion restart')
							.then(() => new Promise((resolve, reject) => {
								let startedAt = Date.now();
								let isRunning = () => {
									this.isRunning()
										.catch((e) => false)	// convert any exception to false
										.then((e) => {
											console.log(`daemonStart: isdaemonrunning?=${e}`)
											if (e) {
												return resolve();
											} 

											if (Date.now() < startedAt + this.options.startWaitMs) {
												setTimeout(isRunning, this.options.startWaitIntervalMs);
											} else {
												reject("Motion daemon did not start in time");
											}
										})
								}

								isRunning();
							}))
							.then(() => this.setDetection(false))
							.then(() => new Promise((resolve) => { 
								setTimeout(() => {
									this.setOutputPictures();
									resolve();
								}, this.options.waitForCameraMs) 
							}))
					});
	}

	/**
	 * Stops the motion daemon
	 * @return a promise
	 */
	stop() {
		return this.options.exec('sudo service motion stop');
	}

	/**
	 * Checks whether detection is active or not
	 * @return a promise(bool) which is resolved if daemon is running, rejected if not running
	 */
	isDetectionActive() {
		return requestpromise(`${MOTION_WEB_URL}/0/detection/status`)
			.then((b) => b.includes('ACTIVE'))
	}

	/**
	 * Sets detection on motion daemon
	 * @returns a promise, which is resolved if done, rejected if daemon is not running
	 */
	setDetection(enabled) {
		const verb = enabled ? 'start' : 'pause';
		console.log(`setDetection: detection = ${verb}`)
		return requestpromise({
			method: 'GET',
			uri: `${MOTION_WEB_URL}/0/detection/${verb}`
		});
	}

	/**
	 * Change `output_pictures` configuration to `on`
	 * It is called after startup to prevent saving pictures while camera is initializing
	 */
	setOutputPictures() {
		return requestpromise({
			method: 'GET',
			uri: `${MOTION_WEB_URL}/0/config/set?output_pictures=on`
		});
	}
};

module.exports.Daemon = Daemon;